-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for cinema
CREATE DATABASE IF NOT EXISTS `cinema` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_general_mysql500_ci */;
USE `cinema`;

-- Dumping structure for table cinema.halls
CREATE TABLE IF NOT EXISTS `halls` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_general_mysql500_ci NOT NULL DEFAULT '',
  `number_of_seats` int(11) NOT NULL,
  `3D` enum('Da','Ne') COLLATE utf8_general_mysql500_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

-- Dumping data for table cinema.halls: ~2 rows (approximately)
/*!40000 ALTER TABLE `halls` DISABLE KEYS */;
INSERT INTO `halls` (`id`, `name`, `number_of_seats`, `3D`) VALUES
	(1, 'Dvorana A', 30, 'Da'),
	(2, 'Dvorana B', 40, 'Ne');
/*!40000 ALTER TABLE `halls` ENABLE KEYS */;

-- Dumping structure for table cinema.movies
CREATE TABLE IF NOT EXISTS `movies` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_general_mysql500_ci NOT NULL DEFAULT '',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `hall_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_movies_halls` (`hall_id`),
  CONSTRAINT `FK_movies_halls` FOREIGN KEY (`hall_id`) REFERENCES `halls` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

-- Dumping data for table cinema.movies: ~11 rows (approximately)
/*!40000 ALTER TABLE `movies` DISABLE KEYS */;
INSERT INTO `movies` (`id`, `name`, `price`, `start_time`, `end_time`, `hall_id`) VALUES
	(1, 'Titanic', 12.00, '2019-09-02 23:00:00', '2019-10-02 01:00:00', 1),
	(2, 'Titanic', 9.00, '2019-09-02 23:00:00', '2019-10-02 01:00:00', 2),
	(3, 'Titanic', 12.00, '2019-09-19 15:00:00', '2019-09-19 17:00:00', 1),
	(4, 'Gospodar prstenova I', 25.00, '2019-09-20 18:00:00', '2019-09-20 21:00:00', 1),
	(5, 'Avatar', 7.00, '2019-09-21 18:00:00', '2019-09-21 20:00:00', 1),
	(51, 'Batman', 10.00, '2019-09-21 18:00:00', '2019-09-21 20:00:00', 2),
	(52, 'Batman', 10.00, '2019-09-22 15:00:00', '2019-09-22 17:00:00', 1),
	(53, 'Matrix', 15.00, '2019-09-23 15:00:00', '2019-09-23 17:00:00', 2),
	(54, 'Goodfellas', 21.00, '2019-09-23 15:00:00', '2019-09-23 17:00:00', 2),
	(55, 'Kum', 24.00, '2019-09-23 11:00:00', '2019-09-23 14:00:00', 2),
	(56, 'Gospodar prstenova II', 25.00, '2019-09-24 11:00:00', '2019-09-24 14:00:00', 1);
/*!40000 ALTER TABLE `movies` ENABLE KEYS */;

-- Dumping structure for table cinema.reservations
CREATE TABLE IF NOT EXISTS `reservations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `movie_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `seat_number` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `movie_id_seat_number` (`movie_id`,`seat_number`),
  KEY `FK_reservations_users` (`user_id`),
  CONSTRAINT `FK_reservations_movies` FOREIGN KEY (`movie_id`) REFERENCES `movies` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_reservations_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

-- Dumping data for table cinema.reservations: ~6 rows (approximately)
/*!40000 ALTER TABLE `reservations` DISABLE KEYS */;
INSERT INTO `reservations` (`id`, `movie_id`, `user_id`, `seat_number`) VALUES
	(28, 4, 11, 1),
	(29, 4, 11, 3),
	(30, 55, 11, 40),
	(31, 55, 11, 26),
	(32, 54, 11, 32),
	(33, 53, 11, 1);
/*!40000 ALTER TABLE `reservations` ENABLE KEYS */;

-- Dumping structure for table cinema.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) COLLATE utf8_general_mysql500_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8_general_mysql500_ci NOT NULL,
  `birth_date` date NOT NULL,
  `email` varchar(50) COLLATE utf8_general_mysql500_ci NOT NULL,
  `password` blob NOT NULL,
  `role` enum('Superadmin','Admin','Customer') COLLATE utf8_general_mysql500_ci NOT NULL DEFAULT 'Customer',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

-- Dumping data for table cinema.users: ~3 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `first_name`, `last_name`, `birth_date`, `email`, `password`, `role`) VALUES
	(11, 'test', 'test', '2019-09-04', 'test', _binary 0x58ADEEE83798D90766446909BAFD9C22, 'Customer'),
	(13, 'Admin', 'Admin', '2019-09-05', 'admin', _binary 0x58ADEEE83798D90766446909BAFD9C22, 'Admin'),
	(14, 'Superadmin', 'Superadmin', '2019-09-05', 'superadmin', _binary 0x58ADEEE83798D90766446909BAFD9C22, 'Superadmin');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
