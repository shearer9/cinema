# Cinema

Application made in JAVA Swing for college assignment.


## Installation

Create database and import included SQL 



```bash
cd src/cinema
open Connectionz.java

DriverManager.getConnection("jdbc:mysql://localhost:3306/{database_name}","{database_user}","{database_pwd");
```

## Usage

There are 3 types of users - Superadmin, Admin and Customer.

Superadmin can create, update and delete movies.

Admin has access to all bookings and he can delete them if there is some reason for it.

Customer can reserve his seat for selected movie.

## License
[MIT](https://choosealicense.com/licenses/mit/)